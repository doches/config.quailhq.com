/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
*/

/*
 Modernizr 3.0.0pre (Custom Build) | MIT
*/

/*
object-assign
(c) Sindre Sorhus
@license MIT
*/

/* ["default"] */

/* default */

/* eslint-disable no-unused-vars */

/* global __REACT_DEVTOOLS_GLOBAL_HOOK__ */

/* harmony default export */

/* harmony export */

/* harmony import */

/* istanbul ignore if  */

/* istanbul ignore next  */

/* module decorator */

/* webpack/runtime/compat get default export */

/* webpack/runtime/publicPath */

/*#__PURE__*/

/**
     * @param withoutSuffix boolean true = a length of time; false = before/after a period of time.
     */

/**
     * Return a human readable representation of a moment that can
     * also be evaluated to get a new moment which is the same
     *
     * @link https://nodejs.org/dist/latest/docs/api/util.html#util_custom_inspect_function_on_objects
     */

/**
     * Returns true if the word before the given number loses the '-n' ending.
     * e.g. 'an 10 Deeg' but 'a 5 Deeg'
     *
     * @param number {integer}
     * @returns {boolean}
     */

/** @license React v0.20.2
 * scheduler.production.min.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

/** @license React v17.0.2
 * react-dom.production.min.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

/** @license React v17.0.2
 * react.production.min.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

/************************************
		Country object
	************************************/

/************************************
		Current Timezone
	************************************/

/************************************
		Global Methods
	************************************/

/************************************
		Interface with Moment.js
	************************************/

/************************************
		Unpacking
	************************************/

/************************************
		Zone object
	************************************/

/************************************
		moment.tz namespace
	************************************/

/************************************************************************/

/******/

/***/

/*global define*/

/*jshint -W100*/

//

// 	logError('Moment Timezone ' + moment.tz.version + ' was already loaded ' + (moment.tz.dataVersion ? 'with data from ' : 'without any data') + moment.tz.dataVersion);

// 	return moment;

//       0 - 9

//       0 - 99

//       0 - 999

//       0 - 9999

//       0 - inf

//      00 - 99

//     000 - 999

//     999 - 9999

//    -inf - inf

//    0000 - 9999

//   99999 - 999999

//  * days do not bubble at all

//  * milliseconds bubble up until they become hours

//  * months bubble up until they become years

// '+10:00' > ['10',  '00']

// '-1530'  > ['-15', '30']

// ()

// (5)

// (fmt)

// (fmt, 5)

// (think of clock changes)

// (true)

// (true, 5)

// (true, fmt)

// (true, fmt, 5)

// * if day of month is given, default month and year

// * if month is given, default only year

// * if no year, month, day of month are given, default to today

// * if year is given, don't default anything

// +00:00 -00:00 +0000 -0000 or Z

// +0200, so we adjust the time as needed, to be valid.

// -999999 - 999999

// 0000-00-00 0000-W00 or 0000-W00-0 + T + 00 or 00:00 or 00:00:00 or 00:00:00.000 + +00:00 or +0000 or +00)

// 1000

// 1000 * 60

// 1000 * 60 * 60

// 1000 * 60 * 60 * 24 * 7, negate dst

// 1000 * 60 * 60 * 24, negate dst

// 12 months -> 1 year

// 123456789 123456789.123

// 17:56:31 CST

// 17:56:31 GMT+0800 (台北標準時間)

// 17:56:31 GMT-0600 (CST)

// 17:56:31 GMT-0600 (Central Standard Time)

// 1af to 10fed

// 2 digits

// 3 or 4 digits --> recursively check first digit

// 3600 seconds -> 60 minutes -> 1 hour

// 400 years have 12 months === 4800

// 400 years have 146097 days (taking into account leap year rules)

// 5:31:26 +0200 It is possible that 5:31:26 doesn't exist with offset

// 9 days / in 9 days / 9 days ago

// 9 hours / in 9 hours / 9 hours ago

// 9 minutes / in 9 minutes / 9 minutes ago

// 9 months / in 9 months / 9 months ago

// 9 seconds / in 9 seconds / 9 seconds ago

// 9 years / in 9 years / 9 years ago

// <input type="date" />

// <input type="datetime-local" />

// <input type="datetime-local" step="0.001" />

// <input type="datetime-local" step="1" />

// <input type="time" />

// <input type="time" step="0.001" />

// <input type="time" step="1" />

// <input type="week" />

// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805

// @see https://github.com/webpack-contrib/style-loader/issues/177

// ALIASES

// ASP.NET json date format regex

// Adding is smart enough around DST

// After the year there should be a slash and the amount of years since December 26, 1979 in Roman numerals.

// Anything larger than 4 digits: recursively check first n-3 digits

// Apply timezone offset from input. The actual utcOffset can be changed

// Because of dateAddRemove treats 24 hours as different from a

// CLDR data:          http://www.unicode.org/cldr/charts/28/summary/ru.html#1753

// CONCATENATED MODULE: ./src/index.tsx

// Check for 24:00:00.000

// Check if module is in cache

// Cloning a moment should include the _z property.

// Code from http://stackoverflow.com/questions/3561493/is-there-a-regexp-escape-function-in-javascript

// Create a new module (and put it into the cache)

// DCE check should happen before ReactDOM bundle executes so that

// Date.UTC remaps years 0-99 to 1900-1999

// Default to current date.

// Default to current week.

// Detect buggy property enumeration order in older V8 versions.

// DevTools can report bad minification during injection.

// DevTools shouldn't crash React, no matter what.

// Different date string for 'Dënschdeg' (Tuesday) and 'Donneschdeg' (Thursday) due to phonological rule

// Do not load moment-timezone a second time.

// EXTERNAL MODULE: ./node_modules/react-dom/index.js

// EXTERNAL MODULE: ./node_modules/react/index.js

// Execute the module function

// Exports

// FORMATTING

// Fallback

// Final attempt, use Input Fallback

// Flag the module as loaded

// For old IE

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>

// GB/T 7408-1994《数据元和交换格式·信息交换·日期和时间表示法》与ISO 8601:1988等效

// Getting start-of-today depends on whether we're local/utc/offset or not.

// Gujarati notation for meridiems are quite fuzzy in practice. While there exists

// HELPERS

// HOOKS

// Hindi notation for meridiems are quite fuzzy in practice. While there exists

// I know

// IE8 Quirks Mode & IE7 Standards Mode do not allow accessing strings like arrays

// IE8 will treat undefined and null as object if it wasn't for

// INJECT DATA

// If passed a locale key, it will set the locale for this

// If url is already wrapped in quotes, remove them

// Intl unavailable, fall back to manual guessing.

// It is impossible to translate months into days without knowing

// It is intended to keep the offset in sync with the timezone.

// Keeping the time actually adds/subtracts (one hour)

// LOCALES

// Lenient ordinal parsing accepts just a number in addition to

// M. E.: those two are virtually unused but a user might want to implement them for his/her website for some reason

// MERGE

// MOMENTS

// Maandag is die eerste dag van die week.

// Math.floor prevents floating point math errors here

// Module

// Moment prototype object

// Moment.js version check

// Monday - Friday

// Monday is the first day of the week.

// NOTE: 'červen' is substring of 'červenec'; therefore 'červenec' must precede 'červen' in the regex to be fully matched.

// Negative Number --> always true

// No op

// Nominativ

// On Firefox.24 Date#getTimezoneOffset returns a floating point.

// Only 1 digit

// Otherwise parser matches '1. červenec' as '1. červen' + 'ec'.

// PARSING

// PRIORITIES

// PRIORITY

// Pass getZone to prevent recursion more than 1 level deep

// Pick a moment m from moments so that m[fn](other) is true for all

// Pick the first defined of two or three arguments.

// Plugins that add properties should also add the key here (null value),

// Prevent infinite loop in case updateOffset creates new moment

// Prevent names that look like filesystem paths, i.e contain '/' or '\'

// Punjabi notation for meridiems are quite fuzzy in practice. While there exists

// RFC 2822 regex: For details see https://tools.ietf.org/html/rfc2822#section-3.3

// Resolves es6 module loading issue

// Return a moment from input, that is local/utc/zone equivalent to model.

// Return the exports of the module

// Saturday is the first day of the week.

// See https://drafts.csswg.org/css-values-3/#urls

// See https://github.com/moment/moment/issues/3375

// Set abbr so it will have a name (getters return

// Setting the hour should keep the time, because the user explicitly

// Should url be wrapped?

// Side effect imports

// Sorting makes sure if one month (or abbr) is a prefix of another

// Sorting makes sure if one month (or abbr) is a prefix of another it

// Sorting makes sure if one weekday (or abbr) is a prefix of another it

// Special case to return head of iframe instead of iframe itself

// Sunday is the first day of the week

// Sunday is the first day of the week.

// Support for single parameter, formats only overload to the calendar function

// TODO: Another silent failure?

// TODO: Find a better way to register and load all the locales in Node

// TODO: Move this to another part of the creation flow to prevent circular deps

// TODO: Remove "ordinalParse" fallback in next major release.

// TODO: Return 'e' when day of month > 1. Move this case inside

// TODO: Use [].sort instead?

// TODO: Use this.as('ms')?

// TODO: We need to take the current isoWeekYear, but that depends on

// TODO: add sorting

// TODO: remove 'name' arg after deprecation is removed

// Test for IE <= 9 as proposed by Browserhacks

// Tests for existence of standard globals is to allow style-loader

// Thanks to Joris Röling : https://github.com/jjupiter

// The following code bubbles up values, see the tests for

// The module cache

// The require function

// There are months name as per Nanakshahi Calendar but they are not used as rigidly in modern Punjabi.

// This array is used to make a Date, either with `new Date` or `Date.UTC`

// This entry need to be wrapped in an IIFE because it need to be in strict mode.

// This function allows you to set a threshold for relative time strings

// This function allows you to set the rounding function for relative time strings

// This function will be called whenever a moment is mutated.

// This function will load locale and then set the global locale.  If

// This is because there is no context-free conversion between hours and days

// This is currently too difficult (maybe even impossible) to add.

// This is done to register the method called with moment()

// This will throw an exception if access to iframe is blocked

// Update existing child locale in-place to avoid memory-leaks

// Use low-level api, because this fn is low-level api.

// Using charAt should be more compatible.

// Verify that the code above has been dead code eliminated (DCE'd).

// We don't expect any of the above to throw, but better to be safe.

// We should still report in case we break this code.

// We want to compare the start of today, vs this.

// We'd normally use ~~inp for this, but unfortunately it also

// When supporting browsers where an automatic publicPath is not supported you must specify an output.publicPath manually via configuration

// Words with feminine grammatical gender: semaine

// Words with masculine grammatical gender: mois, trimestre, jour

// Zero out whatever was not defaulted, including time

// [year, month, day , hour, minute, second, millisecond]

// _changeInProgress == true case, then we have to adjust, because

// a day / in a day / a day ago

// a few seconds / in a few seconds / a few seconds ago

// a few seconds to seconds

// a minute / in a minute / a minute ago

// a month / in a month / a month ago

// a new timezone) makes sense. Adding/subtracting hours does not follow

// a now version of current config (take local/utc/offset flags, and

// a rigid notion of a 'Pahar' it is not used as rigidly in modern Gujarati.

// a rigid notion of a 'Pahar' it is not used as rigidly in modern Hindi.

// a rigid notion of a 'Pahar' it is not used as rigidly in modern Punjabi.

// a second time. In case it wants us to change the offset again

// a year / in a year / a year ago

// actual modulo - handles negative numbers (for dates before 1970):

// add remaining unparsed input length to the string

// affecting the local hour. So 5:31:26 +0300 --[utcOffset(2, true)]-->

// an hour / in an hour / an hour ago

// and also not between days and months (28-31 days per month)

// and further modified to allow for strings containing both week and day

// any word (or two) characters or numbers including two/three word month in arabic.

// apply sign while we're at it

// as a getter, returns 7 instead of 0 (1-7 range instead of 0-6)

// as a setter, sunday should belong to the previous week.

// b is in (anchor - 1 month, anchor + 1 month)

// backwards compat for now: also set the locale

// behaves the same as moment#day except

// block for masculine words below.

// callback: function () { this.month() + 1 }

// can't just apply() to create a date:

// check for mismatching day of week

// check: https://github.com/moment/moment/issues/2166

// checks for null or undefined

// clear _12h flag if hour is <= 12

// compare two arrays, return the number of differences

// constant that refers to the ISO standard

// constant that refers to the RFC 2822 form

// convert an array to a date.

// convert days to months

// converts floats to ints.

// create a top-level div for React to render into

// create now).

// created, so we won't end up with the child locale set.

// css base code, injected by the css-loader

// currently HTML5 input type only supports 24-hour formats

// date and time from ref 2822 format

// date from 1) ASP.NET, 2) ISO, 3) RFC 2822 formats, or 4) optional fallback if parsing isn't strict

// date from iso format

// date from string and array of format strings

// date from string and format string

// day when working around DST, we need to store them separately

// days than the end month.

// days to month/week

// default to beginning of week

// define getter functions for harmony exports

// deprecated in 0.1.0

// difference in months

// don't parse if it's not a known token

// due to cross-origin restrictions

// end-of-month calculations work correct when the start month has more

// eslint-disable-line no-new-wrappers

// eslint-disable-next-line func-names

// eslint-disable-next-line no-param-reassign

// eslint-disable-next-line no-underscore-dangle, no-param-reassign

// eslint-disable-next-line prefer-destructuring

// examples of what that means.

// first element is an array of moment objects.

// first-week day -- which january is always in the first week (4 for iso, 1 for other)

// for ISO strings we do not use the normal bubbling rules:

// format date using native date object

// from http://docs.closure-library.googlecode.com/git/closure_goog_date_date.js.source.html

// from milliseconds

// from the actual represented time. That is why we call updateOffset

// getDefaultExport function for compatibility with non-harmony modules

// goude merenn | a-raok merenn

// handle digits after the decimal

// handle digits before the decimal

// handle era

// handle meridiem

// handle milliseconds separately because of floating point math errors (issue #1867)

// handle negative numbers

// helper function for moment.fn.from, moment.fn.fromNow, and moment.duration.fn.humanize

// hooks is actually the exported moment object

// hours to day

// how we interpret now (local, utc, fixed offset). So create

// http://new.gramota.ru/spravka/rules/139-prop : § 103

// https://bugs.chromium.org/p/v8/issues/detail?id=3056

// https://bugs.chromium.org/p/v8/issues/detail?id=4118

// https://en.wikipedia.org/wiki/ISO_week_date#Calculating_a_date_given_the_year.2C_week_number_and_weekday

// https://github.com/moment/moment/issues/1423

// https://github.com/moment/moment/pull/1871

// https://stackoverflow.com/q/181348

// if (moment.tz !== undefined) {

// if there is any input that was not parsed add a penalty for that format

// if we didn't find a month name, mark the date as invalid.

// if we didn't get a weekday name, mark the date as invalid

// if we have a mix of positive and negative values, bubble down first

// import "core-js";

// import a list of modules into the list

// includes scottish gaelic two word and hyphenated months

// inp may be undefined, so careful calling replace on it.

// input != null

// inspired by https://github.com/dordille/moment-isoduration/blob/master/moment.isoduration.js

// instance.  Otherwise, it will return the locale configuration

// iso 8601 regex

// iso time formats and regexes

// istanbul ignore if

// istanbul ignore next

// it separately.

// keepLocalTime = true means only change the timezone, without

// local weekday -- counting starts from beginning of week

// locale key.

// make sure changes to properties don't modify parent config

// make sure we set the locale AFTER all child locales have been

// make the regex if we don't have it already

// mark as not found to avoid repeating expensive file require call causing high CPU

// match[2] should be 'T' or space

// matching against regexp is expensive, do it on demand

// minutes to hour

// minutes to milliseconds

// moment 2.7.0

// moment 2.8.1+

// moment.duration._locale = moment._locale = data;

// moments should either be an array of moment objects or an array, whose

// native implementation is ~50x faster, use it when we can

// new Date(NaN).toJSON() === null

// no arguments are passed in, it will simply return the current global

// note: all values past the year are optional and will default to the lowest possible value.

// null means not found

// number + (possibly) stuff coming from _dayOfMonthOrdinalParse.

// object construction must be done this way.

// objects.

// only allow non-integers for smallest unit

// or pass an empty string ("") and set the __webpack_public_path__ variable from your code to use your own logic.

// ordinal:  'Mo'

// other. This relies on the function fn to be transitive.

// padded:   ['MM', 2]

// pass null for config to unupdate, useful for tests

// pick the locale from the array

// preserve leap years using a full 400 year cycle, then reset

// refer http://ta.wikipedia.org/s/1er1

// representation for dateAddRemove

// return the list of modules as css string

// returns locale data

// seconds to minute

// see sorting in computeMonthsParse

// so we can properly clone ourselves.

// somewhat more in line with 4.4.3.2 2004 spec, but allows decimal anywhere

// special case for zero

// specified which hour they want. So trying to maintain the same hour (in

// start-of-first-week - start-of-year

// substring from most specific to least, but move to the next array item if it's a more specific variant than the current root

// supports only 2.0-style add(1, 's') or add(duration)

// supports only 2.0-style subtract(1, 's') or subtract(duration)

// tags it will allow on a page

// test the regex

// the Date.UTC function remaps years 0-99 to 1900-1999

// the array should mirror the parameters below

// the date constructor remaps years 0-99 to 1900-1999

// the only allowed military tz is Z

// the ordinal 'er' only applies to day of the month

// the ordinal 'वेर' only applies to day of the month

// the reverse of daysToMonths

// there is no such time in the given timezone.

// this is not used

// this rule.

// time formats are the same as en-gb

// timezone chunker

// to operate correctly into non-standard environments

// token:    'M'

// traditional ordinal numbers above 31 are not commonly used in colloquial Welsh

// truncate time

// try ['en-au', 'en-gb'] as 'en-au', 'en-gb', 'en', as in move through the list trying each

// type MomentInput = Moment | Date | string | number | (number | string)[] | MomentInputObject | void; // null | undefined

// undefined otherwise).

// updateLocale is called for creating a new locale

// use Intl API when available and returning valid time zone

// useful for testing

// variables for this instance.

// webpackBootstrap

// weekday -- low day numbers are considered next week

// weeks to month

// when trying to find en-US, en_US, en-us for every format call

// which months you are are talking about, so we have to store

// will match the longer piece.

// with parseZone.

// without creating circular dependencies.

// yes, three characters difference

// }

// Выражение, которое соответствует только сокращённым формам

// Сокращения месяцев: http://new.gramota.ru/spravka/buro/search-answer?s=242637

// копия предыдущего

// по CLDR именно "июл." и "июн.", но какой смысл менять букву на точку?

// полные названия с падежами

// полные названия с падежами, по три буквы, для некоторых, по 4 буквы, сокращения с точкой и без точки

//! Copyright (c) JS Foundation and other contributors

//! github.com/moment/moment-timezone

//! license : MIT

//! moment-timezone.js

//! moment.js

//! moment.js locale configuration

//! version : 0.5.43

//Different grammatical cases

//check for negative zero, return zero if negative zero

//compute day of the year from weeks and weekdays

//if the day of the year is set, figure out what it is

//invert the arguments, but complain about it

//ne 'diurno', ĉar estas uzita por proksimumo

//or tokens

//short-circuit everything else

//the next array item is better than a shallower substring of this one

//using 1000 * 60 * 60 instead of 36e5 to avoid floating point rounding errors https://github.com/moment/moment/issues/2978

//warn user if arguments are passed but the locale could not be set
