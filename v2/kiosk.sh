#!/bin/bash

# Hide XFCE4 panel
pidof xfce4-panel > /dev/null
if [[ $? -eq 0 ]]; then
  xfce4-panel -q
fi

# clear configured printers
for PRINTER in `sudo lpstat -p | awk '{print $2}'`; do 
	sudo lpadmin -x $PRINTER
done

# Re-configure any attached star printer
PRINTER=`sudo lpinfo -v | grep Star | sed 's/direct //g'`
sudo lpadmin -p startsp -D "Star TSP" -E -m star/tsp143.ppd -v $PRINTER -o CashDrawerSetting=3OpenDrawer3
sudo lpadmin -d startsp
sudo lpoptions -p startsp -o media=Custom.80x140mm -o CashDrawerSetting=3OpenDrawer3
lpoptions -p startsp -o media=Custom.80x140mm -o CashDrawerSetting=3OpenDrawer3

# Start wifi-check
cd wifi_check; nohup sudo node ./bin/www &
sleep 3

# Start Chrome in kiosk mode
chromium "http://localhost:3000" --start-fullscreen --noerrdialogs --disable-translate --no-first-run --fast --fast-start --disable-infobars --disable-features=TranslateUI --allow-insecure-localhost --no-default-browser-check --kiosk --kiosk-printing --use-system-default-printer --password-store=basic