#!/bin/bash
#
# Setup script for the OrangePi PC 800 running Ubuntu + XFCE
#
# Usage: 
#     Right click on the desktop and open a new terminal window. In it, run the following:
#
#     curl -XGET https://config.quailhq.com/v2/setup.sh -o setup.sh
#     chmod +x setup.sh
#     sudo ./setup.sh
#       (root password is 'orangepi')

setup_printer () {
  # Downloading Star printer driver
  echo "Installing Star CUPS driver for ARM"
  rm -rf install/
  rm -f star_tsp.tar.gz
  if [ $1 = 'orangepi-pc-800' ]; then
    curl -s -XGET https://config.quailhq.com/v2/star_tsp_orangepi.tar.gz -o star_tsp.tar.gz
  else
    curl -s -XGET https://config.quailhq.com/v2/star_tsp.tar.gz -o star_tsp.tar.gz
  fi
  tar -zxvf star_tsp.tar.gz
  cd install
  ./setup
  cd ..

  echo "Configuring CUPS to use Star TSP"
  lpadmin -p startsp -D "Star TSP" -E -m star/tsp143.ppd -v `lpinfo -v | grep Star | sed 's/direct //g'` -o CashDrawerSetting=3OpenDrawer3
  echo "Setting Star TSP as default printer"
  lpadmin -d startsp
  echo "Setting default paper size"
  lpoptions -p startsp -o media=Custom.80x140mm -o CashDrawerSetting=3OpenDrawer3
}

setup_wifi_check () {
  # Download latest wifi-check
  echo "Downloading wifi-check"
  rm -f wifi_check.tar.gz
  rm -rf wifi_check/

  curl -s -XGET https://config.quailhq.com/v2/wifi_check-0.0.0-18-g15e019e.tar.gz -o wifi_check.tar.gz
  mkdir -p wifi_check
  mv wifi_check.tar.gz wifi_check/
  cd wifi_check/
  tar -zxvf wifi_check.tar.gz
  cd ..

  # Install nodejs and npm (required for wifi-check)
  echo "Installing wifi-check dependencies (node & npm)"
  apt-get install -y nodejs npm

  # Install deps for wifi-check/
  echo "Building wifi-check"
  cd wifi_check/
  npm install
  cd ..
}

HID="unknown";
HARDWARE_ID_FILE=/opt/register-hardware-id
DEVICE_ID_FILE=/opt/device-id

# Prompt for hardware ID
echo "Select a hardware ID for this device:"
select uhid in "orangepi-pc-800" "bananapi-m2-berry" "orangepi-4-lts; do
  echo "Saving hardware ID: $uhid";
  HID="$uhid"
  break;
done

# Prompt for a device ID
echo "Enter a unique name for this device, e.g. 'acme-register-0':"
read DEVICE_ID

if [ $HID = "orangepi-pc-800" ]; then
  cd /home/orangepi
  usermod -aG sudo orangepi
  if grep NOPASSWD:ALL /etc/sudoers ; then
    echo "Already in sudoers group"
  else
    echo 'orangepi ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
  fi

  # enable older repositories (for old ghostscript)
  if grep bionic /etc/apt/sources.list ; then
    echo "old repository already set up"
  else
    echo "deb http://mirrors.tuna.tsinghua.edu.cn/ubuntu-ports/ bionic main restricted universe multiverse" >> /etc/apt/sources.list
    echo "deb http://mirrors.tuna.tsinghua.edu.cn/ubuntu-ports/ bionic-updates main restricted universe multiverse" >> /etc/apt/sources.list
    
    apt --allow-insecure-repositories update
    
    yes | apt install ghostscript=9.26~dfsg+0-0ubuntu0.18.04.17 libgs9=9.26~dfsg+0-0ubuntu0.18.04.17 libgs9-common=9.26~dfsg+0-0ubuntu0.18.04.17
  fi
  
  # Write hardware ID file
  printf "%s" "$HID" > $HARDWARE_ID_FILE
  chmod 777 $HARDWARE_ID_FILE
  
  # Write device ID file
  printf "%s" "$DEVICE_ID" > $DEVICE_ID_FILE
  chmod 777 $DEVICE_ID_FILE

  # Remove OrangePi branding from startup
  echo "Removing OrangePi branding from startup"
  sed -i 's/bootlogo=true/bootlogo=false/' /boot/orangepiEnv.txt

  # Hide Linux boot console
  echo "Hiding console boot messages"
  sed -i 's/verbosity=1/verbosity=0/' /boot/orangepiEnv.txt

  # Install chrome browser
  echo "Installing Chrome"
  snap install chromium 
  snap disable cups

  # Download desktop wallpaper
  echo "Downloading Quail desktop background"
  curl -s -XGET https://config.quailhq.com/v2/bootsplash.png -o quail.png

  # Set desktop wallpaper
  echo "Applying Quail desktop background"
  mkdir /usr/share/backgrounds/orangepi/backup/
  mv /usr/share/backgrounds/orangepi/*.png /usr/share/backgrounds/orangepi/backup/
  cp quail.png /usr/share/backgrounds/orangepi/orangepi.png
  cp quail.png /usr/share/backgrounds/orangepi/orangepi-default.png
  cp quail.png /usr/share/backgrounds/orangepi/orangepi-mountain.png
  rm quail.png

  # Set menu icon
  echo "Downloading Quail menu icon"
  rm -f quail-icon.png
  curl -s -XGET https://quailhq.com/fonts/favicon@2x.png -o quail-icon.png
  echo "Applying Quail menu icon"
  cp quail-icon.png /usr/share/pixmaps/orangepi/orangepi.png

  # Download kiosk mode script
  echo "Downloading kiosk.sh"
  curl -s -XGET https://config.quailhq.com/v2/kiosk.sh -o /home/orangepi/kiosk.sh
  chmod +x /home/orangepi/kiosk.sh
  chown orangepi /home/orangepi/kiosk.sh
  chgrp orangepi /home/orangepi/kiosk.sh

  # Setting kiosk mode script to run on login
  echo "Configuring kiosk.sh to run on login"
  mkdir -p /home/quail/.config/autostart
  curl -s -XGET https://config.quailhq.com/v2/kiosk.desktop -o /home/orangepi/.config/autostart/kiosk.desktop
  chown orangepi /home/orangepi/.config/autostart/kiosk.desktop
  chgrp orangepi /home/orangepi/.config/autostart/kiosk.desktop

  setup_printer $HID

  setup_wifi_check
  
  #orangepi-specific wifi_check setup
  chown -R orangepi wifi_check/
  chgrp -R orangepi wifi_check/

  echo "Cleaning up"
  rm setup.sh

  echo "Restarting"
  shutdown -r now
elif [ $HID = "bananapi-m2-berry" ]; then
  usermod -aG sudo quail
  if grep NOPASSWD:ALL /etc/sudoers ; then
    echo "Already in sudoers group"
  else
    echo "quail ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
  fi
  
  # Write hardware ID file
  printf "%s" "$HID" > $HARDWARE_ID_FILE
  chmod 777 $HARDWARE_ID_FILE
  
  # Write device ID file
  printf "%s" "$DEVICE_ID" > $DEVICE_ID_FILE
  chmod 777 $DEVICE_ID_FILE
  
  echo "Downloading Quail desktop background"
  curl -s -XGET https://config.quailhq.com/v2/bootsplash.png -o quail.png
  
  echo "Downloading kiosk.sh"
  curl -s -XGET https://config.quailhq.com/v2/kiosk.sh -o kiosk.sh
  chmod +x kiosk.sh
  
  # Setting kiosk mode script to run on login
  echo "Configuring kiosk.sh to run on login"
  mkdir -p /home/orangepi/.config/autostart
  curl -s -XGET https://config.quailhq.com/v2/kiosk.desktop -o /home/quail/.config/autostart/kiosk.desktop
  chown quail /home/quail/.config/autostart/kiosk.desktop
  chgrp quail /home/quail/.config/autostart/kiosk.desktop
  sed -i 's/orangepi/quail/' /home/quail/.config/autostart/kiosk.desktop
  
  echo "Installing Chrome"
  snap install chromium 
  snap disable cups
  
  setup_printer $HID

  setup_wifi_check

  echo "Cleaning up"
  rm setup.sh

  echo "Restarting"
  shutdown -r now
fi