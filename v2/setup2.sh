#!/bin/bash

setup_printer () {
  # Downloading Star printer driver
  echo "Installing Star CUPS driver for ARM"
  rm -rf install/
  rm -f star_tsp.tar.gz
  curl -s -XGET https://config.quailhq.com/v2/star_tsp_orangepi.tar.gz -o star_tsp.tar.gz
  tar -zxvf star_tsp.tar.gz
  cd install
  ./setup
  cd ..

  echo "Configuring CUPS to use Star TSP"
  lpadmin -p startsp -D "Star TSP" -E -m star/tsp143.ppd -v `lpinfo -v | grep Star | sed 's/direct //g'` -o CashDrawerSetting=3OpenDrawer3
  echo "Setting Star TSP as default printer"
  lpadmin -d startsp
  echo "Setting default paper size"
  lpoptions -p startsp -o media=Custom.80x140mm -o CashDrawerSetting=3OpenDrawer3
}

setup_wifi_check () {
  # Download latest wifi-check
  echo "Downloading wifi-check"
  rm -f wifi_check.tar.gz
  rm -rf wifi_check/

  curl -s -XGET https://config.quailhq.com/v2/wifi_check-0.0.0-18-g15e019e.tar.gz -o wifi_check.tar.gz
  mkdir -p wifi_check
  mv wifi_check.tar.gz wifi_check/
  cd wifi_check/
  tar -zxvf wifi_check.tar.gz
  cd ..

  # Install nodejs and npm (required for wifi-check)
  echo "Installing wifi-check dependencies (node & npm)"
  apt-get install -y nodejs npm

  # Install deps for wifi-check/
  echo "Building wifi-check"
  cd wifi_check/
  npm install
  cd ..
}

HARDWARE_ID_FILE=/opt/register-hardware-id
DEVICE_ID_FILE=/opt/device-id

# Prompt for a device ID
echo "Enter a unique name for this device, e.g. 'acme-register-0':"
read DEVICE_ID

# Prompt for a timezone
PS3="Select a timezone: "
options=("Eastern" "Central" "Mountain" "Pacific")
select tz in "${options[@]}"
do
	case $tz in
    	"Eastern")
        	timedatectl set-timezone "America/New_York"; break;;
    	"Central")
        	timedatectl set-timezone "America/Chicago"; break;;
    	"Mountain")
        	timedatectl set-timezone "America/Denver"; break;;
    	"Pacific")
        	timedatectl set-timezone "America/Los_Angeles"; break;;
        *) echo "Invalid option $REPLY";;
    esac
done

# Write hardware ID file
printf "%s" "orangepi-pc-800" > $HARDWARE_ID_FILE
chmod 777 $HARDWARE_ID_FILE

# Write device ID file
printf "%s" "$DEVICE_ID" > $DEVICE_ID_FILE
chmod 777 $DEVICE_ID_FILE

printf "keyauth2" > /opt/register-version
chmod 777 /opt/register-version

snap install chromium 
snap disable cups

# Download kiosk mode script
echo "Downloading kiosk.sh"
rm -f kiosk.sh
curl -s -XGET https://config.quailhq.com/v2/kiosk.sh -o /home/orangepi/kiosk.sh
chmod +x /home/orangepi/kiosk.sh
chown orangepi /home/orangepi/kiosk.sh
chgrp orangepi /home/orangepi/kiosk.sh

# Setting kiosk mode script to run on login
echo "Configuring kiosk.sh to run on login"
mkdir -p /home/quail/.config/autostart
curl -s -XGET https://config.quailhq.com/v2/kiosk.desktop -o /home/orangepi/.config/autostart/kiosk.desktop
chown orangepi /home/orangepi/.config/autostart/kiosk.desktop
chgrp orangepi /home/orangepi/.config/autostart/kiosk.desktop

setup_printer $HID

setup_wifi_check

#orangepi-specific wifi_check setup
chown -R orangepi wifi_check/
chgrp -R orangepi wifi_check/

systemctl disable apport.service

PRIVKEY=/opt/orangepi_pc_key
/bin/cat <<EOT >$PRIVKEY
-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAABlwAAAAdzc2gtcn
NhAAAAAwEAAQAAAYEAp+YZ30m8rkYrdYUVEhgmqf2ksJKr6Gigp78QHroy8fadwmoXzbWt
YYMfdRV0bgsuoH6MZNgm+SMbsKjSkEKLizKDAJDGKasw/IVYvHmlnNUReYWzLuXnVJbniH
Ux83Af9GMRJ/AE0Si2fRG42iT2evJtem9KwTBZVahLlravkrJV+lV0y0Na3B78L5cqCQR6
+NCyqCElTS3XNSUNK6oUxhlxJeu9yTfrZTCdiKkfjNQQXFULCHh5ZmyY7EkWMw8+nThrGV
97HoF2EmfF6u1YqdHJT3i5h8RLPX2Oc5xkREpo0I+pagAScJKeR4XcdaXRaeue7POF4eFL
mxEWUYvZGHTNSrQbX28Oe/dofJOPcqNZsl3lrsfY6dxLI+EtOa6kGQSuz8+u5wOoOHQqLe
35qt8Qw8vwN6HVbehe/g9Tb2DZNFQ465fJ/3ElHWmkGo++PbaiJYcv9hTI/n6ee82MNa+R
z5uCHsaLSH6qKrf4j7ccFfocnFZwNf6cKLaiRBPvAAAFmJrEiKOaxIijAAAAB3NzaC1yc2
EAAAGBAKfmGd9JvK5GK3WFFRIYJqn9pLCSq+hooKe/EB66MvH2ncJqF821rWGDH3UVdG4L
LqB+jGTYJvkjG7Co0pBCi4sygwCQximrMPyFWLx5pZzVEXmFsy7l51SW54h1MfNwH/RjES
fwBNEotn0RuNok9nrybXpvSsEwWVWoS5a2r5KyVfpVdMtDWtwe/C+XKgkEevjQsqghJU0t
1zUlDSuqFMYZcSXrvck362UwnYipH4zUEFxVCwh4eWZsmOxJFjMPPp04axlfex6BdhJnxe
rtWKnRyU94uYfESz19jnOcZERKaNCPqWoAEnCSnkeF3HWl0WnrnuzzheHhS5sRFlGL2Rh0
zUq0G19vDnv3aHyTj3KjWbJd5a7H2OncSyPhLTmupBkErs/PrucDqDh0Ki3t+arfEMPL8D
eh1W3oXv4PU29g2TRUOOuXyf9xJR1ppBqPvj22oiWHL/YUyP5+nnvNjDWvkc+bgh7Gi0h+
qiq3+I+3HBX6HJxWcDX+nCi2okQT7wAAAAMBAAEAAAGBAKfHRr9c3hGAyVjsnjuwqqJgdr
yENQlP1IsMYL1z8AjfGpL2lcbY7+Nrtnm38NsujW+QRhdX8MoxjTAh5BiIhw2YclSUV5G4
IoGVdzEQlrB8MrQvKPmKgHqFOeWgx0OvHh+HB9PTA91HQ+UDWqeTHEOMEl6w7BDVkdAJF1
AKSrTbRROdZdLhAeDJjdS4yCautXwCgSWFydKwJkhTODAUDCJRtkHgq4xyNuatiB7Scla2
a4r46MeULCxwBPjJqEdpyNfQNM+XxzRXwES4hdbZvx1OM513fof2BB7LH2Y0Yp5DUdjXPH
4DjaPOb6/1xPgcQ1zydgeQpocU9IRdfJTzWi5UEmdcMdrWBTCiPmLysg/na+i+kNZeb7mh
8ahf03zGltefiuqk1TED7Sslzj3M15rC6i/SWa/iumpkMkxtHimla/hKX84ic6PCz6ZIDG
5nKK4jWa5M36Gubv2c8ebecmyr3jv0awMP2b2DiDFyje/ldytGfMEgXAd94vyunCBP8QAA
AMABjd22RQafG5eQe17eyR+QQnUEDT29Ks7b1OBXG7GIb54eOhsBQrXWBuUJKgRWK6/2V6
2d4UTxXx/b0NCW8rbLS8KI/ALbSWJtT0ThnZIhD+SGAV4ic1Oh46eC7Ly6bE9+duE1FsnW
Lpz607xUOUdJJ/7yk81gJg44Vvtmk41mO3Nlp8iD5GyDEAi+V8B/mHAVyhSpbEPY4hKnO+
sqFcMT9bnZiWzKMWU/E6EQREJhZ3VzS8YrZiL/z3rKMNSkNqAAAADBANFfhJiTLreJALaQ
tKg4QHTb+LR+rPbDakVTxOAz3lmhtpqtC4Gt2XpU8ziIv9nwEpwTRMx7SLCMze6GG9uRTb
63Ji3RZ+VkXIlClrXte6uSpDgLcHcCJvG0sFilBmyv/7xnkIGgU65389nNgo5uy61EFg/i
WFsrnQeGwd2DC4t2YLqAicGWcwpa0dK5XemX0lQh3egvacoVWwPbPSMkEMa7bv0Qt00i93
G1uLwNuaWuEBwYMMTKcbAuhmeNS+ihbQAAAMEAzUocOKa+22ptOegtxnUuwfXVsDjB1h7A
QXm3vuCMqTkAB7U8a43CAYGdxjSTpTg4xo03pj+Y7BSQlp8JHizVQVcD8CIPpZKkWZvgRg
/jx9ddXFKXqQbFkZuCVIVWFuPX7arMOT/YWS5zbehsDS/OUs/FPZHKk1aqbcBTdK/7t3bm
F8+6W4H6TDmINRqymf80I0MZvw6HFBsSRh0Br6jEAuBsX++L9N0LXOXE568+bX9EJzxqEp
1+gvWoBuBEa01LAAAAG3RyZXZvcmZvdW50YWluQEFzaW1vdi5sb2NhbAECAwQFBgc=
-----END OPENSSH PRIVATE KEY-----
EOT

chmod 600 $PRIVKEY

PUBKEY=/opt/orangepi_pc_key.pub
/bin/cat <<EOT >$PUBKEY
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCn5hnfSbyuRit1hRUSGCap/aSwkqvoaKCnvxAeujLx9p3CahfNta1hgx91FXRuCy6gfoxk2Cb5IxuwqNKQQouLMoMAkMYpqzD8hVi8eaWc1RF5hbMu5edUlueIdTHzcB/0YxEn8ATRKLZ9EbjaJPZ68m16b0rBMFlVqEuWtq+SslX6VXTLQ1rcHvwvlyoJBHr40LKoISVNLdc1JQ0rqhTGGXEl673JN+tlMJ2IqR+M1BBcVQsIeHlmbJjsSRYzDz6dOGsZX3segXYSZ8Xq7Vip0clPeLmHxEs9fY5znGRESmjQj6lqABJwkp5Hhdx1pdFp657s84Xh4UubERZRi9kYdM1KtBtfbw5792h8k49yo1myXeWux9jp3Esj4S05rqQZBK7Pz67nA6g4dCot7fmq3xDDy/A3odVt6F7+D1NvYNk0VDjrl8n/cSUdaaQaj749tqIlhy/2FMj+fp57zYw1r5HPm4IexotIfqoqt/iPtxwV+hycVnA1/pwotqJEE+8= trevorfountain@Asimov.local
EOT

cat $PUBKEY >> .ssh/authorized_keys

TUNNEL_SERVICE=/etc/systemd/system/reverse_tunnel.service
TUNNEL_SCRIPT=/opt/reverse_tunnel.sh

/bin/cat <<EOT >$TUNNEL_SERVICE
[Unit]
Description=Opens a reverse tunnel for Quail to connect to your register
After=network.target network-online.target

[Service]
StandardOutput=journal
ExecStart=${TUNNEL_SCRIPT}
[Install]
WantedBy=default.target
EOT

PORT_NUMBER=$((10000 + $RANDOM % 1000))

/bin/cat <<EOT >$TUNNEL_SCRIPT
#!/bin/bash

sleep 30
ssh -N -R ${PORT_NUMBER}:localhost:22 quail@jumphost.quailhq.com -i ${PRIVKEY} -o StrictHostKeyChecking=no -o ServerAliveInterval=60
EOT

chmod +x $TUNNEL_SCRIPT
systemctl daemon-reload
systemctl enable reverse_tunnel
systemctl start reverse_tunnel

# Embed port number in device id:
DEVICE_ID_SCRIPT=/opt/inject-port-number.sh
/bin/cat <<EOT >$DEVICE_ID_SCRIPT
#!/bin/ruby

lines = IO.readlines("/opt/device-id")
lines.push ARGV[0]
print lines.filter { |l| !l.nil? }.map { |l| l.strip }.join("-")
EOT

chmod +x $DEVICE_ID_SCRIPT
$DEVICE_ID_SCRIPT $PORT_NUMBER > /opt/device-id

echo "Restarting"
shutdown -r now